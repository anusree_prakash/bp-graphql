
import mongoose from 'mongoose';

const { Schema } = mongoose;
const TagSchema = new Schema({
        _id:{
            
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
        }
        ,    
        tagname:{
            type: String,
            required: true,
            trim: true
        },
        createdDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        updatedDate: {
            type: Date,
            default: Date.now,
            index: true,
        }
    });
    export default mongoose.model('Tag',  TagSchema, 'Tag');