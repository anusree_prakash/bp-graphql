/* eslint-disable security/detect-object-injection */
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
const tagResolvers= {
    Query: {
        /*users:async(paren,args,context,info)=>{
            //console.log("context.me 1",context.me)
            //return context.me
            return users
        },*/
        listTags:async(parent,{data},{models})=>{
            console.log("models",models)
            try{
                let tagData=await models.tag.find({}).sort({tagname:1})
                console.log(" tagData",tagData)
                return  tagData
               }
               catch(err){
                console.log("error",err)
                throw new Error(err)
               }
           

        },
        getTag:async(parent,args,{models})=>{
            console.log("args of Tag",args)
            try{
              
                let gettag=await models.tag.findOne({_id:args._id})
                console.log("gettag",gettag)
                return gettag
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
               }
            
        
    },
     listCounttag:async(parent,{data},{models})=>{
        //console.log("models",models)
        try{
            let tagcount=await models.tag.find({}).count()
            console.log("tag count is",tagcount)
            //return commentdata
           }
           catch(err){
            console.log("error",err)
            throw new Error(err)
           }
    }
},  
    Mutation:{
        createTag:async(parent,{data},{models},info)=>{
            try{
               // console.log("data.descri",data.descripti)
                return new models.tag({...data}).save();
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
            }
        },
        
       updateTag:async(parent, { _id, data }, { models }) =>{ 
        try {
            const fields = ["tagname"];
            const tagDoc = await models.tag.findById(_id);
            for (let i = 0; i < fields.length; i += 1) {
                if (data[fields[i]]) tagDoc[fields[i]] = data[fields[i]];
            }
            tagDoc .updatedDate = new Date();
            return tagDoc .save();
        } catch (error) {
            console.log('error', error);
            return error;
        }
       

    },
    deleteTag:async(parent, { _id}, { models }) =>{ 
        try{
        const deltag= await models.tag.deleteOne({_id})
        console.log("deltag",deltag)
        return deltag1
    } catch (error) {
        console.log('error', error);
        return error;
    }
    }
},
Tag:{
    getFeedOfTag:async(parent,args,{models})=>{
        console.log("tag parent",parent)
        try{
          
            let getFeedofdata=await models.feed.find({tagid:parent._id})
            console.log("getFeedofdata",getFeedofdata)
            return getFeedofdata
        }
        catch(err){
            console.log("error",err)
            throw new Error(err)
           }   
    }
}

}

export default tagResolvers