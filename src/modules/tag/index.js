import TagModel from './tagModel';
import TagSchema from './tagSchema';
import TagResolver from './tagResolver';
//import * as LikeLoader from './postLoader';

export default {
    TagModel
    ,
    TagSchema,
    TagResolver
    //LikeLoader,
};
