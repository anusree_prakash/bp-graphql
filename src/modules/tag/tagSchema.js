import { gql } from 'apollo-server-express';

export default gql`
type Tag{
    _id:ID
    tagname:String
    getFeedOfTag:[Feed]
}
type Query{
    listTags:[Tag]
    getTag(_id:ID):Tag
    listCounttag:Tag
}
input CreateTag{
    tagname:String!
}
input UpdateTag{
    tagname:String!
}
type Mutation{
    createTag(data:CreateTag):Tag
    updateTag(_id:ID!,data:UpdateTag):Tag
    deleteTag(_id:ID!):Tag
}`