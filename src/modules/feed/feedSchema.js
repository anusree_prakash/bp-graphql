
import { gql } from 'apollo-server-express';
export default gql`
type Feed{
    userid:ID
    _id:ID
    feedtype:String!
    description:String
    tagid:[ID]
    status:String
    getuser:User  
    commentOfFeed:[Comments]!
    likesOfFeed:[Like]
    FeedTag:[Tag]
    points:Int
   
}
extend type Query{
    listFeed:[Feed]!
    getFeed(_id:ID):Feed!
    listFeedCount:Feed
    listFeedcount:Feed   
}
input CreatePost{
    feedtype:String!
    description:String
    status:String
   
    tagid:[ID] 
  
}
input UpdateFeed{
    feedtype:String
    description:String
  
}
extend type Mutation{
    createPost(data:CreatePost):Feed!
    updateFeed(_id:ID!,data:UpdateFeed):Feed
    deletePost(_id:ID):Feed!
}`;
