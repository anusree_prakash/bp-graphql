import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import {isAuthenticated,isAuthenticatedFeed,isAdmin} from '../auth';
import { combineResolvers } from 'graphql-resolvers';
import emailTemplate from '../email/email-service';

const FeedResolvers= {
    Query: {
        listFeed: combineResolvers(isAuthenticated,async(parent,{data},{models})=>{
            console.log("models",models)
            try{
                let postlist=await models.feed.find({})
                console.log("listFeed",postlist)
                return postlist
               }
               catch(err){
                console.log("error",err)
                throw new Error(err)
               }

            }),
        listFeedcount:async(parent,{data},{models})=>{
            //console.log("models",models)
            try{
                let feedcount=await models.feed.find({}).count()
                console.log("listFeed",feedcount)
               // return postlist
               }
               catch(err){
                console.log("error",err)
                throw new Error(err)
               }
            },
        /*listFeedCount:async(parent,args,context,info)=>{
            //console.log("models",models)
            try{
                let postlistcount=await models.feed.countDocuments({})
                console.log("listFeedcount",postlistcount)
                return postlistcount
               }
               catch(err){
                console.log("error",err)
                throw new Error(err)
               }
        },*/
      
        getFeed: combineResolvers(isAuthenticated, async(parent,args,{models,me})=>{
            console.log("args of getfeed",args)
            try{
              
                let getfeedData=await models.feed.findOne({_id:args._id,userid:me._id})
                console.log("getFeed",getfeedData)
                return getfeedData
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
               }
            
        }),
        listFeedCount:async(parent,args,context,info)=>{

        }
    },
    Mutation:{
            createPost: combineResolvers(isAuthenticated, async(parent,{data},{models,me},info)=>{
                try{
                    //console.log("data.description",data.description)
                    let feeddata=await models.feed({...data,userid:me._id}).save();
                    if(data.feedtype==="text"){
                        let feedData1=await models.feed.findById(data._id)
                        let createpoints1=await models.point({
                            point:1,
                            feedid:feeddata._id,
                           
                            userid:feeddata.userid,
                            
                        }).save()
                            console.log("createpoint",createpoints1)
                    }
                    else if(data.feedtype==="image"){
                        let feedData2=await models.feed.findById(data._id)
                        let createpoints2=await models.point({
                            point:2,
                            feedid:feeddata._id,
                            userid:feeddata.userid
                        }).save()
                    }
                    else{
                        let feedData1=await models.feed.findById(data._id)
                        let createpoints2=await models.point({
                            point:3,
                            feedid:feeddata._id,
                            userid:feeddata.userid
                        }).save()
                    }
                    let notificationOfOwner=await models.notification({
                        text:`${me.firstName} ${me.lastName} shared new post`,
                        feedid:feeddata._id,
                        userid:me._id ,

                        //receivers:[User]
                    }).save()
                    console.log("notificationForOwner",notificationOfOwner)
                    //console.log("createpost",createPost)
                    return feeddata
                }
                catch(err){
                    console.log("error",err)
                    throw new Error(err)
                }
            
            }),
        updateFeed: combineResolvers(isAuthenticatedFeed, async(parent, { _id, data }, { models }) =>{ 
            try {
                let fields = ['description','feedtype'];
                let feedDoc = await models.feed.findById(_id);
                for (let i = 0; i < fields.length; i += 1) {
                    if (data[fields[i]]) feedDoc[fields[i]] = data[fields[i]];
                }
                feedDoc.updatedDate = new Date();
                return feedDoc.save();
            } catch (error) {
                console.log('error', error);
                return error;
            }
        }),
        deletePost:combineResolvers(isAuthenticatedFeed,async(parent,{_id},{models})=>{
    
            try{
                let deletedpost=await models.feed.deleteOne({_id})
                console.log("deletedpost",deletedpost)
                return deletedpost;
    
            
            }catch(error){
                console.log("error",error)
               
                return error;
           }
          }),
    },
        /*deleteFeed:async(parent, { _id, data }, { models }) =>{ 
            try {
                const fields = ['description','feedtype'];
                const feedDoc= await models.feed.deleteOne(_id);
                for (let i = 0; i < fields.length; i += 1) {
                    if (data[fields[i]]) feedDoc[fields[i]] = data[fields[i]];
                }
                feedDoc.updatedDate = new Date();
                return feedDoc.save();
            } catch (error) {
                console.log('error', error);
                return error;
            }
        }*/

    Feed:{
        getuser:async(parent,args,{models})=>{
                console.log("parent of feed",parent)
                try{
                  
                    let getuserdata=await models.user.findOne({_id:parent.userid})
                    console.log("getFeed",getuserdata)
                    return getuserdata
                }
                catch(err){
                    console.log("error",err)
                    throw new Error(err)
                   }   
                },
      
            commentOfFeed:async(parent,args,{models})=>{
                console.log("parent of feed",parent)
                try{
                  
                    let commentofdata=await models.comment.find({feedid:parent._id})
                    console.log("getFeed",commentofdata)
                    return commentofdata
                }
                catch(err){
                    console.log("error",err)
                    throw new Error(err)
                   }   
            },
                likesOfFeed:async(parent,args,{models})=>{
                    console.log("parent of likesoffeed",parent)
                    try{
                      
                        let commentofdata=await models.like.find({feedid:parent._id})
                        console.log("getFeed",commentofdata)
                        return commentofdata
                    }
                    catch(err){
                        console.log("error",err)
                        throw new Error(err)
                       }   
                },
                FeedTag:async(parent,args,{models})=>{
                    console.log("parent of  FeedTag",parent)
                    try{
                      
                        let tagdata=await models.tag.find({_id:{$in:parent.tagid}})
                        console.log("tagdata", tagdata)
                        return tagdata
                    }
                    catch(err){
                        console.log("error",err)
               

            }
            
           
    }
}
}
          
export default FeedResolvers;