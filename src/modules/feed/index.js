import FeedModel from './feedModel';
import FeedSchema from './feedSchema';
import FeedResolver from './feedResolver';
//import * as PostLoader from './postLoader';

export default {
    FeedModel,
    FeedSchema,
    FeedResolver,
    //PostLoader,
};

