import mongoose from 'mongoose';
//import uniqueValidator from 'mongoose-unique-validator';

const { Schema } = mongoose;
const FeedSchema = new Schema({
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
        },
        userid:{
            type: mongoose.Schema.Types.ObjectId,
            auto: true,  
        },
        tagid:[{
            type: mongoose.Schema.Types.ObjectId
        }],
            
        description:{
            type: String,
            required:true

        },
        createdDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        updatedDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        feedtype:{
        type: String,
        enum:['image','video',"text"],
        default:"image",

        }
        ,
     
    status: {
        type: Boolean,
        default: true,
        index: true,
    },
    points:{
        type:Number
    }
    });
    

//PostSchema.plugin(uniqueValidator);

export default mongoose.model('Feed', FeedSchema, 'Feed');