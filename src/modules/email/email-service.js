/* eslint-disable consistent-return */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */
/* eslint-disable security/detect-object-injection */

// const mailgun = require('mailgun-js')({ apiKey: process.env.MAILGUN_API_KEY, domain: process.env.MAILGUN_DOMAIN });
import * as nodemailer from 'nodemailer';

const fs = require('fs');
const mailWorkflow = require('./email-work-flow-data.config.json');

const getEmailTemplate = (eventType, data) => {
    const filePath = `${__dirname}/email-templates/`;
    const event = mailWorkflow[eventType];

    try {
        const filename = filePath + event.templateFileName;
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        let template = fs.readFileSync(filename, { encoding: 'utf-8' });
        for (const i in data) {
            // eslint-disable-next-line security/detect-non-literal-regexp
            template = template.replace(new RegExp(event.fields[i], 'g'), data[i]);
        }
        return template;
    } catch (err) {
        console.log(err);
    }
};

const getEmailSubject = (eventType, data) => {
    const event = mailWorkflow[eventType];

    let subjectText = event.subject;
    for (const i in data) {
        // eslint-disable-next-line security/detect-non-literal-regexp
        subjectText = subjectText.replace(new RegExp(event.fields[i], 'g'), data[i]);
    }
    return subjectText;
};

const sendGoogleMail = (from, to, subject, html) => {
    const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        secure: true,
        port: 465,
        tls: {
            rejectUnauthorized: false,
        },
        auth: {
            user: process.env.SMTP_GOOGLE_MAILER_EMAIL,
            pass: process.env.SMTP_GOOGLE_MAILER_PASSWORD,
        },
    });
    return transporter.sendMail({ from, to, subject, html });
};

// eslint-disable-next-line no-unused-vars
// const sendMailGunMail = (from, to, subject, html) => mailgun.messages().send({ from, to, subject, html });

const sendTemplatedMail = (eventType, data, to) => {
    const template = getEmailTemplate(eventType, data);
    const subject = getEmailSubject(eventType, data);
    return sendGoogleMail(process.env.MAILGUN_FROM_EMAIL, to, subject, template);
};

export default sendTemplatedMail;
