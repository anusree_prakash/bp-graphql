import { gql } from 'apollo-server-express';

export default gql`
type Point{
    _id:ID
    
    feedType:String
    feedid:ID
    userid:ID
    commentid:ID
    likeid:ID
    point:Int

}
type Query{
    listPoints:[Point]
    getPoint(_id:ID):Point
}
input CreatePoint{
    feedType:String
    feedid:Int
    userid:Int
    point:Int
}
type Mutation{
    createPoint(data:CreatePoint):Point!
    deletePoint(_id:ID):Point
}`