import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
const pointResolver= {
    Query: {
        
        listPoints:async(parent,{data},{models})=>{
            console.log("models",models)
            try{
                let pointData=await models.point.find({})
                //console.log(" Data", membersData)
                return  pointData
               }
               catch(err){
                console.log("error",err)
                throw new Error(err)
               }
           

        }
    },
    Mutation:{
        createPoint:async(parent,{data},{models},info)=>{
            try{
               // console.log("data.descri",data.descripti)
                return new models.point({...data}).save();
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
            }
        },
    deletePoint:async(parent, { _id}, { models }) =>{ 
        try{
        const deletpoint= await models.point.deleteOne({_id})
        console.log("deletpoint",deletpoint)
        return deletpoint
    } catch (error) {
        console.log('error', error);
        return error;
        
    }
    }
}
}
export default pointResolver