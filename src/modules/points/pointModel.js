import mongoose from 'mongoose';
const { Schema } = mongoose;
const PointSchema = new Schema({
        _id:{
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
            },
        userid:{
            type: mongoose.Schema.Types.ObjectId,
        },
        feedid:{
            type: mongoose.Schema.Types.ObjectId,
        },
        likeid:{
            type: mongoose.Schema.Types.ObjectId,
           
 //commentid:{
   // type: mongoose.Schema.Types.ObjectId,
 },
        createdDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        updatedDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        feedType:{
                type: String,
                enum:['image','video',"text"],
                default:"text",
        
                
        },
        point:{
            type:Number
        },
     
    });
export default mongoose.model('Points', PointSchema, 'Points');