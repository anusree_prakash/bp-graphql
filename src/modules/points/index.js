import PointModel from './pointModel';
import PointSchema from './pointSchema';
import PointResolver from './pointResolver';
//import * as LikeLoader from './postLoader';

export default {
    PointResolver,
    PointSchema ,
    PointModel
    //LikeLoader,
};