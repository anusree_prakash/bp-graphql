/* eslint-disable security/detect-object-injection */
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import {isAuthenticated,isAuthenticatedMember,isAdmin} from '../auth';
import { combineResolvers } from 'graphql-resolvers';
import emailTemplate from '../email/email-service';
const membersResolver= {
    Query: {
        
        listmembers:combineResolvers(isAuthenticated,async(parent,{data},{models})=>{
            console.log("models",models)
            try{
                let membersData=await models.members.find({})
                console.log(" membersData", membersData)
                return  membersData
               }
               catch(err){
                console.log("error",err)
                throw new Error(err)
               }
           

        })
    },
    Mutation:{
        createMembers:combineResolvers(isAuthenticated,async(parent,{data},{models,me},info)=>{
            try{
               // console.log("data.descri",data.descripti)
                return new models.members({...data,userid:me._id}).save();
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
            }
        }),
    deleteMembers:combineResolvers(isAuthenticatedMember,async(parent, { _id}, { models }) =>{ 
        try{
        const deletdmember= await models.members.deleteOne({_id})
        console.log("deletdmember",deletdmember)
        return deletdmember
    } catch (error) {
        console.log('error', error);
        return error;
    }
    })
},
    Members:{
        userDetails:async(parent,args,{models})=>{
            console.log("members parent",parent)
            try{
              
                let userData=await models.user.find({_id:parent.userid}).sort({firstname:1})
                console.log("userdata",userData)
                return userData
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
               }   
        }
    }
}

export default membersResolver