import MembersModel from './membersModel';
import MembersSchema from './membersSchema';
import MembersResolver from './membersResolver';
//import * as LikeLoader from './postLoader';

export default {
    MembersResolver,
    MembersSchema ,
    MembersModel
    //LikeLoader,
};
