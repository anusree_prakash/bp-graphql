import { gql } from 'apollo-server-express';

export default gql`
type Members{
    _id:ID
    userid:ID
  
    groupid:ID
    userDetails:[User]
}
type Query{
    listmembers:[Members]
}
input CreateMembers{
    groupid:ID
   
}
type Mutation{
    createMembers(data:CreateMembers):Members!
    deleteMembers(_id:ID):Members!
}`