import mongoose from 'mongoose';
const { Schema } = mongoose;
const MembersSchema = new Schema({
        _id:{
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
            },
        userid:{
            type: mongoose.Schema.Types.ObjectId,
        },
        groupid:{
            type: mongoose.Schema.Types.ObjectId,
        }
 ,    

        createdDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        updatedDate: {
            type: Date,
            default: Date.now,
            index: true,
        }
     
    });
export default mongoose.model('Members', MembersSchema, 'Members');