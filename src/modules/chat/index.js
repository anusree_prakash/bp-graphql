import ChatModel from './chatModel';
import ChatSchema from './chatSchema';
import ChatResolver from './chatResolver';
//import * as UserLoader from './userLoader';

export default {
    ChatModel,
    ChatSchema ,
    ChatResolver,
    //UserLoader,
};