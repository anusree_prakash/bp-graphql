/* eslint-disable security/detect-object-injection */
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import {isAuthenticated,isAuthenticatedChat,isAdmin} from '../auth';
import { combineResolvers } from 'graphql-resolvers';
import emailTemplate from '../email/email-service';
const ChatResolver= {
    Query: {
        listChats: combineResolvers(isAuthenticated,async(parent,{data},{models})=>{
            console.log("models",models)
            try{
                let chats=await models.chat.find({})
                console.log("chats",chats)
                return  chats
               }
               catch(err){
                console.log("error",err)
                throw new Error(err)
               }
        }),
        getChat: combineResolvers(isAuthenticated,async(parent,args,{models,me})=>{
                try{
                  
                    let chatData=await models.chat.findOne({_id:args._id,userid:me._id})
                    console.log("chatData",chatData)
                    return chatData
                }
                catch(err){
                    console.log("error",err)
                    throw new Error(err)
                   }
                
            }),
            getCount:async(parent,{data},{models})=>{
                try{
                    let chats=await models.chat.find({}).count()
                    console.log("chats count",chats)
                    return  chats
                   }
                   catch(err){
                    console.log("error",err)
                    throw new Error(err)
                   }
            }
        
    },
    Mutation:{
        createChats: combineResolvers(isAuthenticated,async(parent,{data},{models,me},info)=>{
            try{
               // console.log("data.description",data.description)
                return new models.chat({...data,userid:me._id}).save();
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
            }
        }),
    deletechat: combineResolvers(isAuthenticatedChat,async(parent, { _id}, { models }) =>{ 
        try{
        const deletdmember= await models.chat.deleteOne({_id})
        console.log("deletdmember",deletdmember)
        return deletdmember
    } catch (error) {
        console.log('error', error);
        return error;
    }
    })
},
 Chat:{
         getuserOfchat:async(parent,args,{models})=>{
                //console.log("parent of likesoffeed",parent)
                try{  
                    let chatUserData=await models.user.find({_id:parent.userid})
                    console.log("chatUserData",chatUserData)
                    return chatUserData
                }
                    catch(err){
                    console.log("error",err)
                    throw new Error(err)
                }   
        },
    
        }
    }
export default ChatResolver