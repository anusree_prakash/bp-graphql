import mongoose from 'mongoose';
//import uniqueValidator from 'mongoose-unique-validator';
const { Schema } = mongoose;
const ChatSchema = new Schema({
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
        },
        userid:{
            type: mongoose.Schema.Types.ObjectId,
           
        },
        message:{
            type: String,
            required: true,
            trim: true
        },
        groupID:{
            type: mongoose.Schema.Types.ObjectId,
        }
           , 
        
        createdDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        updatedDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        messageType:{
        type: String,
        enum:['image','video','text'],
        default:"text",

        }
        ,
    status: {
        type: Boolean,
        default: true,
        index: true,
    }
    });
    

//PostSchema.plugin(uniqueValidator);

export default mongoose.model('Chat', ChatSchema, 'Chat');