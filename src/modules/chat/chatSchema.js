import { gql } from 'apollo-server-express';
export default gql`
type Chat{
    _id:ID
    groupID:ID
    userid:ID
    message:String
    messageType:String
    getuserOfchat:User
}
 type Query{
     listChats:[Chat]
     getChat(_id:ID):Chat
     getCount:Chat
 } 
 input CreateChat{
    groupID:ID

    message:String
    messageType:String
 }
 type Mutation{
     createChats(data:CreateChat):Chat!
     deletechat(_id:ID):Chat
 }`