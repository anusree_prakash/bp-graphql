import LikeModel from './likeModel';
import LikeSchema from './likeSchema';
import LikeResolver from './likeResolver';
//import * as LikeLoader from './postLoader';

export default {
    LikeModel,
    LikeSchema,
    LikeResolver,
    //LikeLoader,
};
