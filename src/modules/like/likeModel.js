import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const { Schema } = mongoose;
const LikeSchema = new Schema({
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
        },
        feedid:{
            type: mongoose.Schema.Types.ObjectId
        },
        commentid:{
            type: mongoose.Schema.Types.ObjectId
        },
        userid:{
            type: mongoose.Schema.Types.ObjectId
        },    
        createdDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        updatedDate: {
            type: Date,
            default: Date.now,
            index: true,
        }
     
    });
    



export default mongoose.model('Like',LikeSchema, 'Like');