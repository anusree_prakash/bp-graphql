/* eslint-disable security/detect-object-injection */
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import {isAuthenticated,isAuthenticatedLike,isAdmin} from '../auth';
import { combineResolvers } from 'graphql-resolvers';
import emailTemplate from '../email/email-service';
import { model } from 'mongoose';
const likeResolvers= {
    Query: {
        /*users:async(paren,args,context,info)=>{
            //console.log("context.me 1",context.me)
            //return context.me
            return users
        },*/
        listLike: combineResolvers(isAuthenticated,async(parent,{data},{models})=>{
            console.log("models",models)
            try{
                let listlike=await models.like.find({})
                console.log("listlike",listlike)
                return listlike
               }
               catch(err){
                console.log("error",err)
                throw new Error(err)
               }
           

        }),
        lislikecount:async(parent,{data},{models})=>{
            console.log("models",models)
            try{
                let listlikecount=await models.like.find({}).count()
                console.log("listlike",listlikecount)
                return listlikecount
               }
               catch(err){
                console.log("error",err)
                throw new Error(err)
               }
           

        }
    },
        Mutation:{
            createLike: combineResolvers(isAuthenticated,async(parent,{data},{models,me},info)=>{
                try{
                   // console.log("data.descri",data.descripti)
                    let likedata= await models.like({...data,userid:me._id}).save();
                    //point for owner
                    /*let createpoint=await models.point({
                        point:1,likeid:likedata._id,feedid:likedata.feedid,
                        userid:likedata.userid}).save();
                        console.log("createpoint",createpoint)*/
                        //finding feed*/
                        let feeddata=await models.feed.findById(data.feedid)
                        console.log("feeddata...",feeddata)
                        let pointForowner=await models.point({
                         point:1,
                         
                         userid:feeddata.userid,
                         feedid:feeddata._id,
                         likeid:likedata._id,
                        }).save()
                        let notificationForOwner=await models.notification({
                            text:"liked your post",
                            feedid:feeddata._id,
                            userid:feeddata.userid,
                            senderid:likedata.userid,
                            likeid:likedata._id
                            
                        }).save()
                        /*console.log("pointForReceiver",pointForReciever)
                        let createpoint=await models.point({
                            point:1,
                            likeid:likedata._id,
                            feedid:likedata.feedid,
                            userid:likedata.userid}).save();
                            console.log("createpoint",createpoint)*/
                        

                    //return likedata
                 return likedata

                }
                catch(err){
                    console.log("error",err)
                    throw new Error(err)
                }
            }),
            deleteLike: combineResolvers(isAuthenticatedLike,async(parent, { _id}, { models }) =>{ 
                try{
                const deletedlike= await models.like.deleteOne({_id})
                console.log("data delecomm",deletedlike)
                return deletedlike
            } catch (error) {
                console.log('error', error);
                return error;
            }
            })
    },
    Like:{
        getuserOfLike:async(parent,args,{models})=>{
                console.log("parent of feed",parent)
                try{
                  
                    let likedata=await models.user.findOne({_id:parent.userid})
                    console.log("getFeed",likedata)
                    return likedata
                }
                catch(err){
                    console.log("error",err)
                    throw new Error(err)
                   }   
            },
        },
    }
export default likeResolvers