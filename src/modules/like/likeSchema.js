import { gql } from 'apollo-server-express';

export default gql`
type Like{
    _id:ID
    feedid:ID
    userid:ID
    commentid:ID
    getuserOfLike:User
    point:Int
    

}
type Query{
    listLike:[Like]!
    getLike:Like!
    lislikecount:Like
}
input CreateLike{

    feedid:ID
    commentid:ID
}
type Mutation{
    createLike(data:CreateLike):Like
    deleteLike(_id:ID!):Like
}`