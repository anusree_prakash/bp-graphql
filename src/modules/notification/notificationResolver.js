import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
const notificationResolvers={
Query: {
    listnotification:async(parent,{data},{models})=>{
        console.log("models",models)
        try{
            let listnotif=await models.notification.find({})
            console.log( listnotif)
            return  listnotif
           }
           catch(err){
            console.log("error",err)
            throw new Error(err)
           }
       

    }
},
Mutation: {
    createNotification:async(parent,{data},{models},info)=>{
    try{
       // console.log("data.descri",data.descripti)
        return new models.notification({...data}).save();
    }
    catch(err){
        console.log("error",err)
        throw new Error(err)
    }
}
}
}

export default notificationResolvers