import { gql } from 'apollo-server-express';

export default gql`
type Notification{
    _id:ID
    feedid:ID
    userid:ID
    likeid:ID
    commentid:ID
    text:String
    senderid:ID
    receivers:[User]

}
type Query{
    listnotification:[Notification]

}
input CreateNotification{
    feedid:ID
    userid:ID
    likeid:ID
    commentid:ID
    senderid:ID
}
type Mutation{
    createNotification(data:CreateNotification):Notification
}`