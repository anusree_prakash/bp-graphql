import NotificationModel from './notificationModel';
import NotificationSchema from './notificationSchema';
import NotificationResolver from './notificationResolver';
//import * as LikeLoader from './postLoader';

export default {
    NotificationResolver,
    NotificationSchema ,
    NotificationModel
    //LikeLoader,
};