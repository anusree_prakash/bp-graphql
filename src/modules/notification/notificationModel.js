import mongoose from 'mongoose';
const { Schema } = mongoose;
const NotificationSchema = new Schema({
        _id:{
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
            },
        userid:{
            type: mongoose.Schema.Types.ObjectId,
        },
        feedid:{
            type: mongoose.Schema.Types.ObjectId,
        },
        likeid:{
            type: mongoose.Schema.Types.ObjectId,         
 //commentid:{
   // type: mongoose.Schema.Types.ObjectId,
 },
 text:{
 type:String
 },
 
 senderid:{
    type: mongoose.Schema.Types.ObjectId,
 },
 commentid:{
    type: mongoose.Schema.Types.ObjectId,
 },
        createdDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        updatedDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
     
    });
export default mongoose.model('Notification', NotificationSchema, 'Notification');