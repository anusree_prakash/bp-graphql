import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
const groupResolvers={
    Query:{
        listGroup:async(parent,{data},{models})=>{
            console.log("models",models)
            try{
                let grouplist=await models.group.find({})
                console.log("grouplist",grouplist)
                return grouplist
               }
               catch(err){
                console.log("error",err)
                throw new Error(err)
               }

            },
            getGroup:async(parent,args,{models})=>{
                console.log("args of getfeed",args)
                try{
                  
                    let groupdata=await models.group.findOne({_id:args._id})
                    console.log("getFeed",groupdata)
                    return groupdata
                }
                catch(err){
                    console.log("error",err)
                    throw new Error(err)
                   }
                
            }
    },
    Mutation:{
        createGroup:async(parent,{data},{models},info)=>{
            try{
                //console.log("data.description",data.description)
                return new models.group({...data}).save();
            }
            catch(err){

                console.log("error",err)
                throw new Error(err)
            }

        },

    updateGroup:async(parent, { _id, data }, { models }) =>{ 
        try {
            const fields = ['groupname'];
            const groupDoc= await models.group.findById(_id);
            for (let i = 0; i < fields.length; i += 1) {
                if (data[fields[i]]) groupDoc[fields[i]] = data[fields[i]];
            }
            groupDoc.updatedDate = new Date();
            return groupDoc.save();
        } catch (error) {
            console.log('error', error);
            return error;
        }
    },
    deleteGroup:async(parent, { _id}, { models }) =>{ 
        try{
        const delgroup= await models.group.deleteOne({_id})
        console.log("data delecomm",delgroup)
        return delgroup
    } catch (error) {
        console.log('error', error);
        return error;
    }
    
}
    },
    Group:{
        getGroupmembers:async(parent,args,{models})=>{
            console.log("group",parent)
            try{
              
                let membersData=await models.members.find({groupid:parent._id})
                console.log("membersData",membersData)
                return membersData
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
               }   
        },
        getchats:async(parent,args,{models})=>{
            console.log("group",parent)
            try{
              
                let msg=await models.chat.find({groupID:parent._id})
                console.log("msg",msg)
                return msg
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
               }   
        }
    }
}
export default groupResolvers