import GroupModel from './groupModel';
import GroupSchema from './groupSchema';
import GroupResolver from './groupResolver';
//import * as LikeLoader from './postLoader';

export default {
    GroupModel,
    GroupSchema,
    GroupResolver,
    //LikeLoader,
};