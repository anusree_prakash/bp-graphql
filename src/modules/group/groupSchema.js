import { gql } from 'apollo-server-express';

export default gql`
type Group{
    _id:ID
    creatorid:ID
    groupname:String!
    getGroupmembers:[Members]
    getchats:[Chat]
   
}

type Query{
   listGroup:[Group!]!
   getGroup(_id:ID):Group!
}
input CreateGroup{
    groupname:String!
    creatorid:ID

}
input updateGroup{
    groupname:String!

} 
type Mutation{
    createGroup(data:CreateGroup):Group!
    updateGroup(_id:ID!,data: updateGroup):Group!
    deleteGroup(_id:ID!):Group!

}`;