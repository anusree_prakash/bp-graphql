import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const { Schema } = mongoose;
const GroupSchema = new Schema({
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
        },
        creatorid:{
            type: mongoose.Schema.Types.ObjectId,
        }, 
        createdDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        updatedDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        groupname:{
            type: String,
            required: true,
            trim: true

        }
     
    });
    

GroupSchema.plugin(uniqueValidator);

export default mongoose.model('Group',GroupSchema, 'Group');
