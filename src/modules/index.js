import { gql } from 'apollo-server-express';
import designation from './designation';
import user from './user';
import feed from './feed';
import comment from './comment';
import group from './group';
import like from './like';
import members from './members';
import tag from './tag';
import chat from './chat'
import point from './points'
import media from './media'
import notification from './notification'


const linkSchema = gql`
    type Query {
        _: Boolean
    }

    type Mutation {
        _: Boolean
    }

    type Subscription {
        _: Boolean
    }
`;

const models = {
    user: user.UserModel,
    feed:feed.FeedModel,
    comment:comment.CommentModel,
    designation:designation.DesignationModel,
    like:like.LikeModel,
    group:group.GroupModel,
    members:members.MembersModel,
    tag:tag.TagModel,
    chat:chat.ChatModel,
    point:point.PointModel,
    media:media.MediaModel,
    notification:notification.NotificationModel
};

export default {
    models,
    schema: [linkSchema, user.UserSchema,feed.FeedSchema,
        designation.DesignationSchema,
        comment.CommentSchema,members.MembersSchema,
        notification.NotificationSchema,
        like.LikeSchema,group.GroupSchema,tag.TagSchema,chat.ChatSchema,point.PointSchema,media.MediaSchema],
    resolvers: [user.UserResolver,feed.FeedResolver,
        designation.DesignationResolver,
        notification.NotificationResolver,
        comment.CommentResolver,like.LikeResolver,group.GroupResolver,members.MembersResolver,
    tag.TagResolver,chat.ChatResolver,point.PointResolver,
media.MediaResolver],
    loaders: {
        user: (keys) => user.userLoader.batchUsers(keys, models),
    },
};
