import mongoose from 'mongoose';
//import uniqueValidator from 'mongoose-unique-validator';

const { Schema } = mongoose;
const CommentSchema = new Schema({
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
        },
            
        text:{
            type: String,
            
            trim: true

        },
        createdDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        updatedDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
    status: {
        type: Boolean,
        default: true,
        index: true,
    },
    feedid:{
        type: mongoose.Schema.Types.ObjectId,
            auto: true,
    },
    userid:{
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    }
    });
    
//CommentSchema.plugin(uniqueValidator);

export default mongoose.model('Comment', CommentSchema, 'Comments');