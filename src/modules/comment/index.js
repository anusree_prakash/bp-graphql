import CommentModel from './commentModel';
import CommentSchema from './commentSchema';
import CommentResolver from './commentResolver';
//import * as UserLoader from './userLoader';

export default {
    CommentModel,
    CommentSchema,
    CommentResolver,
    //UserLoader,
};
