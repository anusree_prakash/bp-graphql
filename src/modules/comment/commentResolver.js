import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import {isAuthenticated,isAuthenticatedComment,isAdmin} from '../auth';
import { combineResolvers } from 'graphql-resolvers';
import emailTemplate from '../email/email-service';
const commentResolvers={
Query: {
    
    listcomment: combineResolvers(isAuthenticated,async(parent,{data},{models})=>{
        console.log("models",models)
        try{
            let commentdata=await models.comment.find({})
            console.log("listcomment",commentdata)
            return commentdata
           }
           catch(err){
            console.log("error",err)
            throw new Error(err)
           }
    }),
    listcommentcount:async(parent,{data},{models})=>{
        //console.log("models",models)
        try{
            let commentcount=await models.comment.find({}).count()
            console.log("listcomment",commentcount)
            //return commentdata
           }
           catch(err){
            console.log("error",err)
            throw new Error(err)
           }
    },
    getcomment: combineResolvers(isAuthenticated,async(parent,args,{models})=>{
        console.log("args of getfeed",args)
        try{
          
            let getCommentData=await models.comment.findOne({_id:args._id})
            console.log("getComment",getCommentData)
            return getCommentData
        }
        catch(err){
            console.log("error",err)
            throw new Error(err)
           }
})
},
Mutation:{
    createComment: combineResolvers(isAuthenticated,async(parent,{data},{models,me},info)=>{
        console.log("data of createComment",{data})
        try{
           
            //console.log("data.commentOfFeed",data.text)
            //return new models.comment({ ...data}).save();
            //console.log("data.description",data.description)
            let commentdata=await models.comment({...data,userid:me._id}).save();
            let feeddata=await models.feed.findById(data.feedid)
            //postowner
            let createpointsforPost=await models.point({
                point:2,
                feedid:feeddata._id,
                userid:feeddata.userid,
            }).save()
            //comment
            let pointforcommentowner=await models.point({
                point:1,
                feedid:commentdata._id,
                userid:commentdata.userid,
            }).save()
            let notificationForOwner=await models.notification({
                text:`${me.firstName} ${me.lastName} commented your post`,
               //text:"commented your post",
                feedid:feeddata._id,
                userid:feeddata.userid,
                senderid:commentdata.userid
            }).save()
            console.log("notification",notificationForOwner)
            return commentdata
        }
        


            
            //const data={
               //description:args.input.description
              
        catch(err){
        console.log("error",err)
        throw new Error(err)
        }
              }),
              updateCommen: combineResolvers(isAuthenticatedComment,async(parent, { _id, data }, { models}) =>{ 
                console.log("data.text",data.text)
                try {
                    const fields = ['text'];
                    const commentDoc= await models.comment.findById(_id);
                    for (let i = 0; i < fields.length; i += 1) {
                        if (data[fields[i]]) commentDoc[fields[i]] = data[fields[i]];
                    }
                    commentDoc.updatedDate = new Date();
                    console.log("commentDoc",commentDoc)
                    console.log(data.text)
                    return commentDoc.save();

                } catch (error) {
                    console.log('error', error);
                    return error;
                }
            }),
            deletecomment: combineResolvers(isAuthenticatedComment,async(parent, { _id}, { models }) =>{ 
                try{
                const commentDoc1= await models.comment.deleteOne({_id})
                console.log("data delecomm",commentDoc1)
                return commentDoc1
            } catch (error) {
                console.log('error', error);
                return error;
            }
            })
},
Comments:{
    getFeedOfcomment:async(parent,{ _id, data },{ models }) =>{ 
        try{
        const feedData= await models.feed.findOne({_id:parent.feedid})
        console.log("commentDoc2", feedData)
        //console.log("data delecomm",data)
        return feedData
    } catch (error) {
        console.log('error', error);
        return error;
    }
    }
    
}

}
export default commentResolvers