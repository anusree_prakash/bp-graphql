
import { gql } from 'apollo-server-express';
export default gql`
type Comments{
    _id:ID
    userid:ID
    tagid:ID
    text:String!
    status:String
    feedid:ID!
    getFeedOfcomment:Feed!
    point:Int

}
extend type Query{
    listcomment:[Comments]!
    getcomment(_id:ID):Comments
    listcommentcount:Feed
} 
input CreateComment{
    text:String!
 
    feedid:ID 
}
input UpdateComment{
    text:String!
}

extend type Mutation{
    createComment(data:CreateComment):Comments!
    updateCommen(_id:ID!,data:UpdateComment):Comments!
    deletecomment(_id:ID!):Comments!


}`
