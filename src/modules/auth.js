import { skip, combineResolvers } from 'graphql-resolvers';

export const isAuthenticated = async (parent, args, context) => {
    console.log("context",context)
    if (context.me) {
        const user = await context.models.user.findById(context.me.userId);
        console.log("context.me.userId",context.me.userId)
        if (!user.status) return new Error('User has no permissions');

        context.me = user;
        return user ? skip : new Error('Not authenticated')

    }
    throw new Error('Not authenticated');
};

export const isAdmin = combineResolvers(isAuthenticated, async (parent, args, { me }) =>
me.role === 'admin' ? skip : new Error('Not authenticated')


);
export const isAuthenticatedFeed= combineResolvers(isAuthenticated,async (parent, {_id},{models,me}) => {
   if(me.role==='admin') return skip
    const feed=await models.feed.findById(_id)
    if(!feed) return "feed does not exist"
    if(feed.userid.toString()===me._id.toString()) return skip;
    else return new Error('Not authenticated');
});

export const isAuthenticatedComment= combineResolvers(isAuthenticated,async (parent,{_id},{models,me},info) => {
    if(me.role==='admin')return skip
    const comment=await models.comment.findById(_id,{userid:1})
    if(!comment) return "comment does not exist"
    if(comment.userid.toString()===me._id.toString()) return skip;
    else return new Error('Not authenticated');
})
export const isAuthenticatedLike= combineResolvers(isAuthenticated,async (parent,{_id},{models,me},info) => {
    if(me.role==='admin')return skip
    const like=await models.like.findById(_id,{userid:1})
    if(!like) return "like does not exist"
    if(like.userid.toString()===me._id.toString()) return skip;
    else return new Error('Not authenticated');
})
export const isAuthenticatedMember= combineResolvers(isAuthenticated,async (parent,{_id},{models,me},info) => {
    if(me.role==='admin')return skip
    const member=await models.members.findById(_id,{userid:1})
    if(!member) return "member does not exist"
    if(member.userid.toString()===me._id.toString()) return skip;
    else return new Error('Not authenticated');
})
export const isAuthenticatedChat= combineResolvers(isAuthenticated,async (parent,{_id},{models,me},info) => {
    if(me.role==='admin')return skip
    const chat=await models.chat.findById(_id,{userid:1})
    if(!chat) return "chat does not exist"
    if(chat.userid.toString()===me._id.toString()) return skip;
    else return new Error('Not authenticated');
})