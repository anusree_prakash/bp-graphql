import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';

const desigResolvers = {
    Query: {
        listDesignation:async(parent,{data},{models})=>{
        console.log("models",models)
        try{
            let desigList=await models.designation.find({})
            console.log("listFeed",desigList)
            return desigList
           }
           catch(err){
            console.log("error",err)
            throw new Error(err)
           }

        },
        getDesignation:async(parent,args,{models})=>{
            console.log("args of getfeed",args)
            try{
              
                let getDesigdata=await models.designation.findOne({_id:args._id})
                console.log("getComment",getDesigdata)
                return getDesigdata
            }
            catch(err){
                console.log("error",err)
            
                throw new Error(err)
               }
    },
    
        listcountOfDesignation:async(parent,{data},{models})=>{
        console.log("models",models)
        try{
            let countOfDesignation=await models.designation.find({}).count()
            console.log("listFeed",countOfDesignation)
            return countOfDesignation
           }
           catch(err){
            console.log("error",err)
            throw new Error(err)
           }

        }
    }, 
    Mutation:{
        createdesignation:async(parent,{data},{models},info)=>{
            try{
                console.log("data",{data})
                return new models.designation({...data}).save();
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
            }
        },
        deleteDesignation:async(parent, { _id}, { models }) =>{ 
            try{
            const deleteddata= await models.designation.deleteOne({_id})
            console.log("data delecomm",deleteddata)
            return deleteddata
        } catch (error) {
            console.log('error', error);
            return error;
        }
        },
        updateDesignation:async (parent, { _id, data }, { models }) => {
            //console.log("data 1,data")
            try {
                const fields = ['title'];
                const designationDoc = await models.designation.findById(_id);
                for (let i = 0; i < fields.length; i += 1) {
                    if (data[fields[i]]) designationDoc[fields[i]] = data[fields[i]];
                }
                designationDoc.updatedDate = new Date();
                return designationDoc.save();
            } catch (error) {
                console.log('error', error);
                return error;
            }
        }
   
},
Designation:{
    getfeedOfDesignation:async(parent,args,{models})=>{
        console.log("parent of feed",parent)
        try{
          
            let designationFeed=await models.feed.findOne({userid:parent.userid})
            console.log("getFeed",designationFeed)
            return designationFeed
        }
        catch(err){
            console.log("error",err)
            throw new Error(err)
           }   
        }
},

}
export default desigResolvers