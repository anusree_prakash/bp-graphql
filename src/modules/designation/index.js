import DesignationModel from './desigModel';
import DesignationSchema from './desigSchema';
import DesignationResolver from './desigResolver';
//import * as UserLoader from './userLoader';

export default {
    DesignationModel,
    DesignationSchema,
    DesignationResolver,
    //UserLoader,
};
