import mongoose from 'mongoose';


const { Schema } = mongoose;
const DesignationSchema = new Schema({
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
        },
            
        title:{
            type: String,
            required: true,
            trim: true

        },
        status: {
            type: Boolean,
            default: true,
            index: true,
        }
    });
    



export default mongoose.model('Designation', DesignationSchema, 'Designation');