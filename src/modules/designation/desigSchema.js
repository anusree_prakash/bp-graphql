import { gql } from 'apollo-server-express';

export default gql`
type Designation{
    _id:ID
    title:String
    status:String
    getfeedOfDesignation:Feed
}
type Query{
    listDesignation:[Designation]!
    getDesignation(_id:ID!):Designation
    listcountOfDesignation:Designation
}
input createDesignation {
    title:String
 
   
}
input UpdateDesignation{
    title:String
}
type Mutation{
    createdesignation(data:createDesignation):Designation!
    deleteDesignation(_id:ID!):Designation
    updateDesignation(_id:ID!,data:UpdateDesignation):Designation


}`;
