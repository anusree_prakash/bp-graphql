/* eslint-disable no-console */
/* eslint-disable security/detect-object-injection */
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
var ObjectId = require('mongodb').ObjectID;

const UserResolvers = {
    Query: {
        listUser: combineResolvers(isAuthenticated, async (parent, args, { models }) => {
            console.log("models",models)
            const data = await models.user.find(
                { status: true },
                { _id: 1, firstName: 1,userDesignation:1,feedid:1, lastName: 1, email: 1, phoneNumber: 1, role: 1, status: 1 }
            );
            console.log("data",data)
            return data;
        }),
          
        getUser:async(parent,args,{models})=>{
            console.log("args of getfeed",args)
            try{
              
                let getuserData=await models.user.findOne({_id:args._id})
                console.log("getFeed",getuserData)
                return getuserData
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
               }
            
        },
        me:combineResolvers(isAuthenticated, async (parent, args, context) => {
            console.log("context",context)
            console.log("context.me",context.me)
            return context.me
            //const userdata=await models.user.findOne({
                //_id:context.me._id
        })
           // const userdata = await context.models.user.findById(context.me.userId);
            //console.log("userdata",userdata)
           // return context.me     
    },
    Mutation: {
        login:async(parent,{email,password},{models})=>{
            const findUser = await models.user.findOne({ email }, { _id: 1, password: 1 });
            if (!findUser) return new Error('User does not exist');

            const isEqual = await bcrypt.compare(password, findUser.password);
            if (!isEqual) return new Error('Email/Password does not match');

            const token = jwt.sign({ userId: findUser._id }, process.env.JWT_SECRET, {
                expiresIn: process.env.JWT_EXPIRY,
            });
            return { token };
        },
        createUser: async (parent, { data }, { models }) => {
            try {
                const hashedPassword = await bcrypt.hash(
                    data.password,
                    Number(process.env.BCRYPT_PASSWORD_HASH_ROUNDS)
                );
                return new models.user({ ...data, password: hashedPassword }).save();
            } catch (error) {
                console.log('error', error);
                return error;''
            }
        },
        updateUser: combineResolvers(isAuthenticated, async (parent, { _id, data }, { models }) => {
            console.log("data 1,data")
            try {
                const fields = ['firstName', 'lastName', 'email', 'phoneNumber','userDesignation'];
                const registrationDoc = await models.user.findById(_id);
                for (let i = 0; i < fields.length; i += 1) {
                    if (data[fields[i]]) registrationDoc[fields[i]] = data[fields[i]];
                }
                registrationDoc.updatedDate = new Date();
                return registrationDoc.save();
            } catch (error) {
                console.log('error', error);
                return error;
            }
        }),
        forgotPassword: async (parent, { email }, { models }) => {
            try {
                const foundUser = await models.user.findOne({ email, status: true }, { firstName: 1, lastName: 1 });
                if (!foundUser) return new Error('User does not exist');

                const resetPasswordToken = jwt.sign({ userId: foundUser._id }, process.env.JWT_SECRET, {
                    expiresIn: process.env.JWT_FORGET_PASSWORD_EXPIRY,
                });
                foundUser.resetPasswordToken = resetPasswordToken;
                await foundUser.save();
                const name = foundUser.firstName;
                await emailTemplate(
                    'forgotPassword',
                    { name, url: `${process.env.CLIENT_WEB_APP_URL}/#/resetPasswordToken` },
                    email
                );
                return { status: true, message: `Password reset email has been sent to ${email}` };
            } catch (error) {
                console.log('error', error);
                return error;
            }
        },
        changePassword: combineResolvers(
            isAuthenticated,
            async (parent, { currentPassword, newPassword }, { models, me: { _id } }) => {
                try {
                    const findUser = await models.user.findById(_id, { password: 1 });
                    if (!findUser) return new Error('User does not exist');

                    const isEqual = await bcrypt.compare(currentPassword.trim(), findUser.password);
                    if (!isEqual) return new Error('Password is incorrect');

                    const hashedPassword = await bcrypt.hash(
                        newPassword.trim(),
                        Number(process.env.BCRYPT_PASSWORD_HASH_ROUNDS)
                    );
                    findUser.password = hashedPassword;
                    findUser.resetPasswordToken = null;
                    await findUser.save();

                    return { status: true, message: 'Password has been updated successfully' };
                } catch (error) {
                    console.log('error', error);
                    return error;
                }
            }
        ),
    },
        User:{
            getFeedOfuser:async(parent,args,{models})=>{
                console.log("parent of user",parent)
                try{
                  
                    let getFeedOfuser=await models.feed.find({userid:parent._id})
                    console.log("getFeed",getFeedOfuser)
                    return getFeedOfuser
                }
                catch(err){
                    console.log("error",err)
                    throw new Error(err)
                   }   
            }
            ,
        getDesignation:async(parent,args,{models})=>{
           // console.log("parent of user",parent)
            try{
              
                let getdesign=await models.designation.find({_id:parent.userDesignation})
                console.log("getFeed",getdesign)
                return  getdesign
            }
            catch(err){
                console.log("error",err)
                throw new Error(err)
               }   
        },
        getChats:async(parent,args,{models})=>{
            // console.log("parent of user",parent)
             try{
               
                 let getchats=await models.chat.find({userid:parent._id})
                 console.log("getchats",getchats)
                 return  getchats
             }
             catch(err){
                 console.log("error",err)
                 throw new Error(err)
                }   
         },
         getrank:async(parent,args,{models})=>{
            // console.log("parent of user",parent)
             try{     
      
            let rank = await models.point.aggregate([
                {
                    $group: {
                        _id: '$userid',
                        totalPoint: { $sum: '$point' },
                    },
                },
                { $sort: { totalPoint: 1 } },
                {
                    $group: {
                        _id: false,
                        arr: {
                            $push: {
                                userid: '$_id',
                                totalPoint: '$totalPoint',
                            },
                        },
                    },
                },
                {
                    $unwind: {
                        path: '$arr',
                        includeArrayIndex: 'rank',
                    },
                },
                { $match: { 'arr.userid':ObjectId(`${parent._id}`) } },
                { $set: { rank: { $add: ['$rank', 1] } } },
                { $project: { rank: 1, _id: 0 } },
            ]);
            
            console.log("rank",rank)
            return rank
             }
             catch(err){
                 console.log("error",err)
                 throw new Error(err)
                }   
         
    } 
    
}

//db.Points.aggregate({["$group":{"_id":{"feedid":"$feedid"},"total points":{"$sum":"$point"}}}])db.Points.aggregate({["$group":{"_id":{"feedid":"$feedid"},"total points":{"$sum":"$point"}}}])
    };
export default UserResolvers;
