import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
const mediaResolvers={
Query: {
    listMedia:async(parent,{data},{models})=>{
        console.log("models",models)
        try{
            let listmedia=await models.media.find({})
            console.log("listmedia",listmedia)
            return listmedia
           }
           catch(err){
            console.log("error",err)
            throw new Error(err)
           }
       

    }
},
Mutation: {
createMedia:async(parent,{data},{models},info)=>{
    try{
       // console.log("data.descri",data.descripti)
        return new models.media({...data}).save();
    }
    catch(err){
        console.log("error",err)
        throw new Error(err)
    }
}
}
}

export default mediaResolvers