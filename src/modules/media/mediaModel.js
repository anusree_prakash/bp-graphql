import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const { Schema } = mongoose;
const MediaSchema = new Schema({
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            auto: true,
        },
        feedtype:{
            type: String,
            enum:['image','video'],
            default:"image",
    
            }, 
            url:{
                type: String,
            }  ,
            feedid:{
                type: mongoose.Schema.Types.ObjectId
            } ,
        createdDate: {
            type: Date,
            default: Date.now,
            index: true,
        },
        updatedDate: {
            type: Date,
            default: Date.now,
            index: true,
        }
     
    });
    



export default mongoose.model('Media',MediaSchema, 'Media');