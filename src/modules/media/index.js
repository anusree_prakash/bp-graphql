import MediaModel from './mediaModel';
import MediaSchema from './mediaSchema';
import MediaResolver from './mediaResolver';
//import * as LikeLoader from './postLoader';

export default {
    MediaModel,
    MediaSchema,
    MediaResolver,
    //LikeLoader,
};
