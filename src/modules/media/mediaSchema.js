import { gql } from 'apollo-server-express';

export default gql`
type Media{
    _id:ID
    feedid:ID
    feedtype:String
    url:String

}
type Query{
    listMedia:[Media]
}
input CreateMedia{
    feedid:ID
    url:String
    feedtype:String
}
type Mutation{
    createMedia(data:CreateMedia):Media
}`