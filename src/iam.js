import jwt from 'jsonwebtoken';

const userAuth = async (token) => {
    try {
        const verifiedData = jwt.verify(token, process.env.JWT_SECRET);
        console.log("verifiedData ",verifiedData)
        return verifiedData;
    } catch (e) {
        return null;
        // throw new AuthenticationError('User Token invalid');
    }
};
export default async (req) => {
    const usertoken = req.headers.authorization;
    if (usertoken) return userAuth(usertoken);
    return null;
};
