/* eslint-disable no-shadow */
import cors from 'cors';
import { ApolloServer } from 'apollo-server-express';
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core';
import express from 'express';
import http from 'http';
import modules from './modules';
import dbconnect from './dbconnect';
import iam from './iam';
import getLoaders from './get-loaders';

async function startApolloServer(typeDefs, resolvers) {
    // Required logic for integrating with Express
    const app = express();
    app.use(cors());

    const httpServer = http.createServer(app);

    // Same ApolloServer initialization as before, plus the drain plugin.
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
        tracing: true,
        cacheControl: { defaultMaxAge: 3 },
        context: async ({ req }) => {
            const me = await iam(req);
            const loaders = getLoaders();
            return {
                me,
                models: modules.models,
                loaders,
            };
        },
    });

    await dbconnect();

    // More required logic for integrating with Express
    await server.start();
    server.applyMiddleware({
        app,

        // By default, apollo-server hosts its GraphQL endpoint at the
        // server root. However, *other* Apollo Server packages host it at
        // /graphql. Optionally provide this to match apollo-server.
        path: '/',
    });
    const port = process.env.PORT || 4000;
    // Modified server startup
    await new Promise((resolve) => httpServer.listen({ port }, resolve));
    // eslint-disable-next-line no-console
    console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`);
}

startApolloServer(modules.schema, modules.resolvers);
